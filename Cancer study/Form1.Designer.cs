﻿namespace Cancer_study
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend3 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.tbage = new System.Windows.Forms.TextBox();
            this.tbmenopause = new System.Windows.Forms.TextBox();
            this.menopause = new System.Windows.Forms.Label();
            this.tbinvnodes = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbtumorsize = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tbbreastquad = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tbbreast = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tbdegmalig = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tbnodecaps = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.tbirradiat = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.lbresult1 = new System.Windows.Forms.Label();
            this.lbresult = new System.Windows.Forms.Label();
            this.linkLabel = new System.Windows.Forms.LinkLabel();
            this.label10 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(424, 409);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(104, 29);
            this.button1.TabIndex = 0;
            this.button1.Text = "SUBMIT";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(27, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "age";
            // 
            // tbage
            // 
            this.tbage.Location = new System.Drawing.Point(142, 41);
            this.tbage.Name = "tbage";
            this.tbage.Size = new System.Drawing.Size(100, 22);
            this.tbage.TabIndex = 2;
            // 
            // tbmenopause
            // 
            this.tbmenopause.Location = new System.Drawing.Point(142, 80);
            this.tbmenopause.Name = "tbmenopause";
            this.tbmenopause.Size = new System.Drawing.Size(100, 22);
            this.tbmenopause.TabIndex = 4;
            // 
            // menopause
            // 
            this.menopause.AutoSize = true;
            this.menopause.Location = new System.Drawing.Point(27, 80);
            this.menopause.Name = "menopause";
            this.menopause.Size = new System.Drawing.Size(82, 17);
            this.menopause.TabIndex = 3;
            this.menopause.Text = "menopause";
            // 
            // tbinvnodes
            // 
            this.tbinvnodes.Location = new System.Drawing.Point(142, 161);
            this.tbinvnodes.Name = "tbinvnodes";
            this.tbinvnodes.Size = new System.Drawing.Size(100, 22);
            this.tbinvnodes.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(27, 161);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 17);
            this.label3.TabIndex = 7;
            this.label3.Text = "inv-nodes";
            // 
            // tbtumorsize
            // 
            this.tbtumorsize.Location = new System.Drawing.Point(142, 122);
            this.tbtumorsize.Name = "tbtumorsize";
            this.tbtumorsize.Size = new System.Drawing.Size(100, 22);
            this.tbtumorsize.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(27, 122);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(74, 17);
            this.label4.TabIndex = 5;
            this.label4.Text = "tumor-size";
            // 
            // tbbreastquad
            // 
            this.tbbreastquad.Location = new System.Drawing.Point(142, 319);
            this.tbbreastquad.Name = "tbbreastquad";
            this.tbbreastquad.Size = new System.Drawing.Size(100, 22);
            this.tbbreastquad.TabIndex = 16;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(27, 319);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(85, 17);
            this.label5.TabIndex = 15;
            this.label5.Text = "breast-quad";
            // 
            // tbbreast
            // 
            this.tbbreast.Location = new System.Drawing.Point(142, 280);
            this.tbbreast.Name = "tbbreast";
            this.tbbreast.Size = new System.Drawing.Size(100, 22);
            this.tbbreast.TabIndex = 14;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(27, 280);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(48, 17);
            this.label6.TabIndex = 13;
            this.label6.Text = "breast";
            // 
            // tbdegmalig
            // 
            this.tbdegmalig.Location = new System.Drawing.Point(142, 238);
            this.tbdegmalig.Name = "tbdegmalig";
            this.tbdegmalig.Size = new System.Drawing.Size(100, 22);
            this.tbdegmalig.TabIndex = 12;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(27, 238);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(70, 17);
            this.label7.TabIndex = 11;
            this.label7.Text = "deg-malig";
            // 
            // tbnodecaps
            // 
            this.tbnodecaps.Location = new System.Drawing.Point(142, 199);
            this.tbnodecaps.Name = "tbnodecaps";
            this.tbnodecaps.Size = new System.Drawing.Size(100, 22);
            this.tbnodecaps.TabIndex = 10;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(27, 199);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(75, 17);
            this.label8.TabIndex = 9;
            this.label8.Text = "node-caps";
            // 
            // tbirradiat
            // 
            this.tbirradiat.Location = new System.Drawing.Point(142, 361);
            this.tbirradiat.Name = "tbirradiat";
            this.tbirradiat.Size = new System.Drawing.Size(100, 22);
            this.tbirradiat.TabIndex = 18;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(27, 361);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(52, 17);
            this.label9.TabIndex = 17;
            this.label9.Text = "irradiat";
            // 
            // lbresult1
            // 
            this.lbresult1.AutoSize = true;
            this.lbresult1.Location = new System.Drawing.Point(320, 21);
            this.lbresult1.Name = "lbresult1";
            this.lbresult1.Size = new System.Drawing.Size(63, 17);
            this.lbresult1.TabIndex = 19;
            this.lbresult1.Text = "RESULT";
            this.lbresult1.Visible = false;
            // 
            // lbresult
            // 
            this.lbresult.AutoSize = true;
            this.lbresult.Location = new System.Drawing.Point(334, 46);
            this.lbresult.Name = "lbresult";
            this.lbresult.Size = new System.Drawing.Size(0, 17);
            this.lbresult.TabIndex = 20;
            // 
            // linkLabel
            // 
            this.linkLabel.AutoSize = true;
            this.linkLabel.Location = new System.Drawing.Point(248, 9);
            this.linkLabel.Name = "linkLabel";
            this.linkLabel.Size = new System.Drawing.Size(11, 17);
            this.linkLabel.TabIndex = 21;
            this.linkLabel.TabStop = true;
            this.linkLabel.Text = "i";
            this.linkLabel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel_LinkClicked);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(144, 9);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(98, 17);
            this.label10.TabIndex = 22;
            this.label10.Text = "Enter the data";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(142, 396);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(100, 23);
            this.button2.TabIndex = 23;
            this.button2.Text = "RESET";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // chart1
            // 
            chartArea3.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea3);
            legend3.Name = "Legend1";
            this.chart1.Legends.Add(legend3);
            this.chart1.Location = new System.Drawing.Point(337, 92);
            this.chart1.Name = "chart1";
            series3.ChartArea = "ChartArea1";
            series3.Color = System.Drawing.Color.Blue;
            series3.Legend = "Legend1";
            series3.Name = "Class";
            this.chart1.Series.Add(series3);
            this.chart1.Size = new System.Drawing.Size(451, 300);
            this.chart1.TabIndex = 24;
            this.chart1.Text = "chart1";
            this.chart1.Visible = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSkyBlue;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.chart1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.linkLabel);
            this.Controls.Add(this.lbresult);
            this.Controls.Add(this.lbresult1);
            this.Controls.Add(this.tbirradiat);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.tbbreastquad);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.tbbreast);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.tbdegmalig);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.tbnodecaps);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.tbinvnodes);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tbtumorsize);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tbmenopause);
            this.Controls.Add(this.menopause);
            this.Controls.Add(this.tbage);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "CANCER STUDY";
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbage;
        private System.Windows.Forms.TextBox tbmenopause;
        private System.Windows.Forms.Label menopause;
        private System.Windows.Forms.TextBox tbinvnodes;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbtumorsize;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbbreastquad;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbbreast;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbdegmalig;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tbnodecaps;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tbirradiat;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lbresult1;
        private System.Windows.Forms.Label lbresult;
        private System.Windows.Forms.LinkLabel linkLabel;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
    }
}

