﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cancer_study
{
    public partial class Form1 : Form
    {
        int numcancer=0;
        int numnotcancer=0;
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            InvokeRequestResponseService();
            lbresult1.Visible = true;
            lbresult.Visible = true;
            chart1.Visible = true;
            




        }
        private async Task InvokeRequestResponseService()
        {
            using (var client = new HttpClient())
            {
                var scoreRequest = new
                {

                    Inputs = new Dictionary<string, StringTable>() {
                        {
                            "input1",
                            new StringTable()
                            {
                                ColumnNames = new string[] {"Class", "age", "menopause", "tumor-size", "inv-nodes", "node-caps", "deg-malig", "breast", "breast-quad", "irradiat"},
                                Values = new string[,] {  { "0", tbage.Text, tbmenopause.Text, tbtumorsize.Text, tbinvnodes.Text,tbnodecaps.Text, tbdegmalig.Text, tbbreast.Text,tbbreastquad.Text, tbirradiat.Text }  }
                            }
                        },
                    },
                    GlobalParameters = new Dictionary<string, string>()
                    {
                    }
                };
                const string apiKey = "KlADbMfh9BxP5mHutVnp0XNGiXSm7XOcHJnCwyNQEjbHKC9UiI1pQ9ht50T2xWnevCAFs3rZNMyV99De/pwEsw=="; // Replace this with the API key for the web service
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", apiKey);

                client.BaseAddress = new Uri("https://ussouthcentral.services.azureml.net/workspaces/879d9ff6b8c64edf804c2143ae05a438/services/b8b0dee6b6444c7bbc3cbdfcca7e8529/execute?api-version=2.0&details=true");

                // WARNING: The 'await' statement below can result in a deadlock if you are calling this code from the UI thread of an ASP.Net application.
                // One way to address this would be to call ConfigureAwait(false) so that the execution does not attempt to resume on the original context.
                // For instance, replace code such as:
                //      result = await DoSomeTask()
                // with the following:
                //      result = await DoSomeTask().ConfigureAwait(false)


                HttpResponseMessage response = await client.PostAsJsonAsync("", scoreRequest);

                if (response.IsSuccessStatusCode)
                {
                    string result = await response.Content.ReadAsStringAsync();
                    // MessageBox.Show(result);

                    if (result[result.Length - 8].ToString() == "0")
                    {
                        numnotcancer++;
                        lbresult.Text = "The current results show that you do not have cancer, but " + Environment.NewLine + " we certainly recommend routine control.";
                    }
                    else
                    {
                        lbresult.Text = "CAUTION! Please contact your doctor as soon as possible to make " + Environment.NewLine + " a detailed examination";
                        numcancer++;
                    }
                    chart1.Series["Class"].Points.Clear();
                    chart1.Series["Class"].Points.AddXY("Cancer", numcancer);
                    chart1.Series["Class"].Points.AddXY("No Cancer", numnotcancer);
                }
                else
                {
                    Console.WriteLine(string.Format("The request failed with status code: {0}", response.StatusCode));

                    // Print the headers - they include the requert ID and the timestamp, which are useful for debugging the failure
                    Console.WriteLine(response.Headers.ToString());

                    string responseContent = await response.Content.ReadAsStringAsync();
                    Console.WriteLine(responseContent);
                    MessageBox.Show(responseContent.ToString());

                }
            }
        }




        public class StringTable
        {
            public string[] ColumnNames { get; set; }
            public string[,] Values { get; set; }
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void linkLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("https://archive.ics.uci.edu/ml/datasets/breast+cancer");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            lbresult1.Visible = false;
            lbresult.Visible = false;
            // tbage.Text, tbmenopause.Text, tbtumorsize.Text, tbinvnodes.Text,tbnodecaps.Text, tbdegmalig.Text, tbbreast.Text,tbbreastquad.Text, tbirradiat.Text
            tbage.Text = "";
            tbmenopause.Text = "";
            tbtumorsize.Text = "";
            tbinvnodes.Text = "";
            tbinvnodes.Text = "";
            tbdegmalig.Text = "";
            tbbreast.Text = "";
            tbbreastquad.Text = "";
            tbirradiat.Text = "";
            tbnodecaps.Text = "";
            chart1.Visible = false;


        }
    }
}
